package flashcards;

import java.io.*;
import java.util.*;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static int askCounter = 0;
    private static int savedCards = 0;
    private static int loadedCards = 0;

    public static void main(String[] args) {
        Map<String, String> cards = new TreeMap<>();
        Map<String, Integer> errors = new TreeMap<>();
        ArrayList<String> logs = new ArrayList<>();

        checkImport(cards, errors,logs, args);
        getChoice(cards, errors, logs, args);
    }

    public static void getChoice(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs, String[] args) {

        while(true) {
            System.out.println("Input the action (add, remove, import, export, ask, exit, log, hardest card, reset stats):");
            logs.add("Input the action (add, remove, import, export, ask, exit, log, hardest card, reset stats):");
            String action = scanner.nextLine();
            logs.add(action);

            switch(action) {
                case "add":
                    addCard(cards, errors, logs);
                    break;
                case "remove":
                    removeCard(cards, errors, logs);
                    break;
                case "import":
                    importCards(cards, errors, logs, args);
                    break;
                case "export":
                    exportCards(cards, errors, logs, args);
                    break;
                case "ask":
                    int times = getTimes(logs);
                    askDefinitions(cards, errors, times, logs);
                    break;
                case "exit":
                    System.out.println("Bye bye!\n");
                    logs.add("Bye bye!\n");
                    checkExport(cards, errors, logs, args);
                    return;
                case "log":
                    printLogs(logs);
                    break;
                case "hardest card":
                    hardestCard(errors, logs);
                    break;
                case "reset stats" :
                    resetStats(errors, logs);
                    break;
            }
        }
    }

    public static void addCard(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs) {
        System.out.println("The card:");
        logs.add("The card:");
        String question = scanner.nextLine();
        logs.add(question);
        if(cards.containsKey(question)) {
            System.out.println("The card " + "\"" + question + "\"" + " already exists.\n");
            logs.add("The card " + "\"" + question + "\"" + " already exists.\n");
            return;
        }

        System.out.println("The definition of the card:");
        logs.add("The definition of the card:");
        String answer = scanner.nextLine();
        logs.add(answer);
        if(cards.containsValue(answer)) {
            System.out.println("The definition " + "\"" + answer + "\"" + " already exists.\n");
            logs.add("The definition " + "\"" + answer + "\"" + " already exists.\n");
            return;
        }

        cards.put(question, answer);
        errors.put(question, 0);
        System.out.println("The pair (" + "\"" + question + "\"" + ":" + "\"" + answer + "\"" + ") has been added.\n");
        logs.add("The pair (" + "\"" + question + "\"" + ":" + "\"" + answer + "\"" + ") has been added.\n");
    }

    public static void removeCard(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs) {
        System.out.println("The card:");
        logs.add("The card:");
        String question = scanner.nextLine();
        logs.add(question);
        if(cards.keySet().contains(question)) {
            cards.remove(question);
            errors.remove(question);
            System.out.println("The card has been removed.\n");
            logs.add("The card has been removed.\n");
        } else {
            System.out.println("Can't remove " + "\"" + question + "\"" + ": there is no such card.\n");
            logs.add("Can't remove " + "\"" + question + "\"" + ": there is no such card.\n");
        }
    }

    public static void importCards(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs, String[] args) {

        String fileName = "";
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-import")) {
                fileName = args[i+1];
            }
        }

        if(fileName.equals("")) {
            System.out.println("File name:");
            logs.add("File name:");
            fileName = scanner.nextLine();
            logs.add(fileName);
        }

        File file = new File("./" + fileName);

        try (Scanner scanner = new Scanner(file)) {
            while(scanner.hasNext()) {
                String question = scanner.nextLine();
                String answer = scanner.nextLine();
                int mistakes = scanner.nextInt();
                scanner.nextLine();

                cards.put(question, answer);
                errors.put(question, mistakes);

                loadedCards++;
            }
        } catch(FileNotFoundException e) {
            System.out.println("File not found.\n");
            logs.add("File not found.\n");
            loadedCards = 0;
            return;
        }

        System.out.println(loadedCards + " cards have been loaded\n");
        logs.add(loadedCards + " cards have been loaded\n");
        loadedCards = 0;
    }

    public static void exportCards(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs, String[] args) {

        String fileName = "";
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-export")) {
                fileName = args[i+1];
            }
        }

        if(fileName.equals("")) {
            System.out.println("File name:");
            logs.add("File name:");
            fileName = scanner.nextLine();
            logs.add(fileName);
        }

        File file = new File("./" + fileName);

        try (PrintWriter printWriter = new PrintWriter(file)) {
            cards.forEach((question, answer) -> {
                printWriter.println(question);
                printWriter.println(answer);

                errors.forEach((card, err) -> {
                    if(card.equals(question)) {
                        printWriter.println(err);
                    }
                });

                savedCards++;
            });
        } catch (IOException e) {
            System.out.println("An exception occurs, " + e.getMessage());
            logs.add("An exception occurs, " + e.getMessage());
        }

        System.out.println(savedCards + " cards have been saved\n");
        logs.add(savedCards + " cards have been saved\n");
        savedCards = 0;
    }

    public static int getTimes(ArrayList<String> logs) {
        System.out.println("How many times to ask?");
        logs.add("How many times to ask?");
        int cardAmount = scanner.nextInt();
        scanner.nextLine();
        return cardAmount;
    }

    public static void askDefinitions(Map<String, String> cards, Map<String, Integer> errors, int times, ArrayList<String> logs) {

        askCounter = 0;
        while(askCounter < times) {

            for(Map.Entry<String, String> entry : cards.entrySet()) {

                askCounter++;

                System.out.println("Print the definition of " + "\"" + entry.getKey() + "\"" + ":");
                logs.add("Print the definition of " + "\"" + entry.getKey() + "\"" + ":");
                String definition = scanner.nextLine();
                logs.add(definition);

                if(entry.getValue().equals(definition)) {
                    System.out.println("Correct answer.");
                    logs.add("Correct answer.");
                } else {
                    if(cards.containsValue(definition)) {

                        cards.forEach((k, v) -> {
                            if(v.equals(definition)) {
                                String key = k;
                                System.out.println("Wrong answer. The correct one is " + "\"" + entry.getValue() + "\"" + ", you've just written the definition of " +
                                        "\"" + key + "\"" + ".");
                                logs.add("Wrong answer. The correct one is " + "\"" + entry.getValue() + "\"" + ", you've just written the definition of " +
                                        "\"" + key + "\"" + ".");
                            }
                        });

                    } else {
                        System.out.println("Wrong answer. The correct one is " + "\"" + entry.getValue() + "\"" + ".");
                        logs.add("Wrong answer. The correct one is " + "\"" + entry.getValue() + "\"" + ".");
                    }

                    for(Map.Entry<String, Integer> item : errors.entrySet()) {
                        if(item.getKey().equals(entry.getKey())) {
                            errors.replace(item.getKey(), item.getValue(), item.getValue() + 1);
                        }
                    }
                }

                if(askCounter == times) break;
            }
        }
    }

    public static void printLogs(ArrayList<String> logs) {
        System.out.println("File name:");
        String fileName = scanner.nextLine();
        File file = new File("./" + fileName);

        try (FileWriter writer = new FileWriter(file)) {
            logs.forEach(item -> {
                try {
                    writer.write(item + "\n");
                } catch (IOException e) {
                    System.out.println("An exception occurred, " + e.getMessage());
                }

            });
        } catch (IOException e) {
            System.out.println("An exception occurred, " + e.getMessage());
            return;
        }

        System.out.println("The log has been saved.\n");
    }

    public static void hardestCard(Map<String, Integer> errors, ArrayList<String> logs) {
        ArrayList<String> hardestCards = new ArrayList<>();
        Formatter formatter = new Formatter();

        int highestAmountOfErrors = 0;
        for(Map.Entry<String, Integer> entry : errors.entrySet()) {
            if(entry.getValue() > highestAmountOfErrors) {
                highestAmountOfErrors = entry.getValue();
            }
        }

        for(Map.Entry<String, Integer> entry : errors.entrySet()) {
            if(entry.getValue() == highestAmountOfErrors) {
                hardestCards.add(entry.getKey());
            }
        }

        if(highestAmountOfErrors == 0) {
            System.out.println("There are no cards with errors.\n");
            logs.add("There are no cards with errors.\n");
        } else {
            if(hardestCards.size() == 1) {
                System.out.printf("The hardest card is %s. You have %d errors answering it.\n", getHardestCards(hardestCards), highestAmountOfErrors);
                logs.add(formatter.format("The hardest card is %s. You have %d errors answering it.\n", getHardestCards(hardestCards), highestAmountOfErrors).toString());
            } else {
                System.out.printf("The hardest cards are %s. You have %d errors answering them.\n", getHardestCards(hardestCards), highestAmountOfErrors);
                logs.add(formatter.format("The hardest cards are %s. You have %d errors answering them.\n", getHardestCards(hardestCards), highestAmountOfErrors).toString());
            }
        }
    }

    public static String getHardestCards(ArrayList<String> hardestCards) {
        Formatter formatter = new Formatter();

        for(int i = 0; i < hardestCards.size(); i++) {

            if(i == hardestCards.size() -1) {
                formatter.format("\"" + hardestCards.get(i) + "\"");
            } else {
                formatter.format("\"" + hardestCards.get(i) + "\"" + ", ");
            }
        }

        return formatter.toString();
    }

    public static void resetStats(Map<String, Integer> errors, ArrayList<String> logs) {
        for(Map.Entry<String, Integer> entry : errors.entrySet()) {
            errors.put(entry.getKey(), 0);
        }

        System.out.println("Card statistics has been reset.\n");
        logs.add("Card statistics has been reset.\n");
    }

    public static void checkImport(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs, String[] args) {
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-import")) {
                importCards(cards, errors, logs, args);
            }
        }
    }

    public static void checkExport(Map<String, String> cards, Map<String, Integer> errors, ArrayList<String> logs, String[] args) {

        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-export")) {
                exportCards(cards, errors, logs, args);
            }
        }
    }
}